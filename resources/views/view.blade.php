<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Hello Mr/Miss/Mrs {{$data->name}}</h2>
  <a class="btn btn-info" style="float: right;margin-bottom: 2px" href="{{route('alldata')}}" role="button">Back</a>
           
  
        <p>your name is {{$data->name}}</p>
        <p>Your email is: {{$data->email}}</p>
        <p>Your Roll Is: {{$data->roll}}</p>
     
</div>

</body>
</html>
