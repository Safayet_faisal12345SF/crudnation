<!DOCTYPE html>
<html>
    <head>
        <title>My first layout</title>
        <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    </head>
    <body>
        <header class="header" >
        	
        	@yield('header')


        </header>
        <nav>
            <ul class="list">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('/about')}}">about us</a></li>
                
                <li><a href="{{url('/contact')}}">contact us</a></li>

            </ul>

        </nav>


      @yield('main')


        <footer>&copy; 2019 hello bangladesh</footer>
    </body>
</html>