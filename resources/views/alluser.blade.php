<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script src="https:////cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>
<body>

<div class="container">
  <h2>All Students</h2>
              
  <table class="table" id="myTable">
    <thead>
      <tr>
        <th>Id</th>
        <th>students_id</th>
        <th>images</th>
        
        <th>Create</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

     @foreach($data as $yameen)
      <tr>
        <td>{{$yameen->id}}</td>
        <td>{{$yameen->name}}</td>
        <td><img src="{{url($yameen->images)}}" style="width: 300px; height: 200px"></td>
       
        <td>{{$yameen->created_at}}</td>
        <td>
          <a class="btn btn-primary" href="{{url('image/view/'.$yameen->id)}}" role="button">View</a>
          <a class="btn btn-warning" href="{{url('/image/edit/'.$yameen->id)}}" role="button">Edit</a>
          <a class="btn btn-danger" href="{{url('/image/delete/'.$yameen->id)}}" role="button">Delete</a>
</td>
      </tr>
      
      @endforeach
    </tbody>
  </table>
</div>
<script type="text/javascript">
	$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>

<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
</body>
</html>
