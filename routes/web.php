<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


/* Store data */

Route::get('/contact',function(){
	return view('pages.contact');
});
Route::post('/storessss','HomeController@store')->name('store');
//Alldata view
Route::get('/alldata','HomeController@index')->name('alldata');
//Single Data view
Route::get('/view/{id}','HomeController@show');
Route::get('/delete/{id}','HomeController@destroy');
Route::get('/edit/{id}','HomeController@edit');
Route::post('/update/{id}','HomeController@update');

//image upload
Route::get('/image/reg/', 'HomeController@reg');
//Route::get('/registation', 'HomeController@reg');
Route::post('reg/store','HomeController@regstore')->name('reg.store');

Route::get('/image/all', 'HomeController@ImageAll')->name('alluser');
Route::get('/image/view/{id}', 'HomeController@ImgView');
Route::get('/image/delete/{id}', 'HomeController@ImgDelete');
Route::get('/image/edit/{id}', 'HomeController@ImgEdit');
Route::post('/image/update/{id}', 'HomeController@ImgUpdate');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
